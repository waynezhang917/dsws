# -*- coding: utf-8 -*-
import logging
import time
import datetime as dt
from datetime import datetime
from dateutil.relativedelta import relativedelta

from varyag.officers import step as officer_step
from varyag.officers import law_office as officer_law_office
from varyag.officers import kernel as officer_kernel

logger = logging.getLogger(__name__)
global_logger = logging.getLogger("varyag")

mon_dict = {
    'Jan': '01',
    'Feb': '02',
    'Mar': '03',
    'Apr': '04',
    'May': '05',
    'Jun': '06',
    'Jul': '07',
    'Aug': '08',
    'Sep': '09',
    'Oct': '10',
    'Nov': '11',
    'Dec': '12'
}

test_law_firms = {
    1: "developer",
    3: "Test",
    7: "Test Law Firm",
    24: "USA Law Group",
    29: "Task Template Law Group",
    30: "AILawTest",
    38: "AddPartnerGroup",
    40: "TEST AD",
    42: "Test-Tianzesb",
    80: "Vendor Trial 01",
    112: "AILaw Support"
}

deprecated_law_firms = {
    21: "WHOmentors.com"
}

potential_law_firms = {
    92: "Frank & Delaney Immigration",
    188: "",
    288: "Ayuda Center",
    289: "Franco Law Firm, P.A.",
    290: "BKR Immigration"
}

paid_law_firms = {
    2: "DeHeng Law Offices",
    4: "LyD Law",
    5: "YC Law Group, PC",
    8: "Law Offices of Kelly H. Bu",
    10: "SAC ATTORNEYS LLP",
    16: "Carrier and Allison Law Group",
    17: "Hsiang Law Group",
    23: "LAW OFFICE OF YUANYUE MU PLLC",
    27: "Occam Immigration",
    34: "Stelmakh & Associates LLC",
    55: "Valerio Spinaci, Esq.",
    59: "JT Law Group",
    64: "Huntley PC",
    76: "Calderon Law Firm, LLC",
    83: "Ashoori Law",
    95: "Law Firm of Anish Vashistha, APLC",
    107: "Corporate Immigration Law Firm",
    109: "AFAR IMMIGRATION SERVICE",
    114: "D & T Law Corporation",
    121: "HAYMAN-WOODWARD",
    125: "Robert G. Schrader, Esq.",
    130: "Streit & Su",
    131: "The Law Firm of Edward Rodriguez, P.C",
    135: "The Mitzel Group, LLP",
    136: "Deborah J. Townsend, P.A.",
    142: "Olvera & Associates",
    144: "Knezek Law",
    145: "The Pilkington Law Firm",
    150: "Ian Li Law",
    153: "Wang, Leonard & Condon, Attorneys at Law",
    154: "Immigration Legal Options",
    155: "BBI Law Group, P.C.",
    156: "Desiree Dominguez Immigration Law Office",
    159: "Camacho   Bridges",
    161: "Comprehensive Immigration Solutions, LLC",
    162: "Path Law Group",
    163: "Murray & Silva, P.A.",
    164: "Law Office of Paul C. Agu",
    165: "Ceraulo Zhang LLP",
    167: "Elissa I. Henry Law Firm, PLLC",
    174: "Alicia C. Armstrong Law Office",
    175: "Ballon Stoll Bader & Nadler, P.C.",
    178: "Law Offices of William Kiang",
    179: "Ardila Law Firm",
    181: "The Law Firm of Moumita Rahman, PLLC",
    182: "Leiva Law Firm",
    183: "Michael G. Murray, P.A.",
    184: "Moreno Law",
    185: "Kassamanian, Kodchian & Associates",
    186: "The Byars Firm",
    189: "MCWHIRTER LAW FIRM, PLLC",
    190: "Law Office of Isaias Torres",
    191: "Law Office Of Kisuk Paek",
    192: "Immigration Law Office of Claire Degerin",
    195: "Leleu & Associates, LLC",
    197: "Lydia Kitahara",
    198: "Berner Immigration Law Firm",
    199: "SBC ILDC",
    201: "Islas-Muñoz Law Firm",
    202: "Margaret W. Wong & Associates, LLC",
    203: "Quest It Solutions, Inc",
    204: "Ybarra Maldonado Law Group",
    207: "The Schaefer Law Firm Pllc",
    208: "The Law Office of M. Ray Arvand, P.C.",
    209: "BMM TAX SERVICES LLC",
    210: "Law Offices of Kevin R. Riva",
    212: "BMP Consulting Group",
    214: "Meng Law Group PC",
    217: "Matten Law",
    220: "Law Offices of Victoria V Kuzmina",
    221: "Francis Law Center",
    222: "Haglund Law Firm",
    223: "Robb Hill Attorney At Law PLLC",
    224: "RJS Law Group",
    227: "Moreno Colón PLLC",
    229: "Apply Usa Visa LLC",
    230: "Leaf, Ferreira, de Araujo, LLC",
    231: "Taxes David",
    232: "Benme Legal",
    235: "Rambana & Ricci, P.L.L.C.",
    238: "Jean Law Group, LLC",
    241: "Maui Immigration Law",
    242: "Hall Law Office, PLLC",
    244: "Fay Grafton Nunez",
    245: "MindQuest Technology Solutions LLC",
    248: "Pollak PLLC",
    249: "Meehan & Naveed",
    250: "Castro Legal Group",
    252: "Leone Zhgun",
    253: "Arnold L. Feldman",
    254: "The Shagin Law Group LLC",
    256: "Mirabile Law Firm",
    258: "Kasper & Frank LLP",
    259: "Gee & Zhang, llp",
    260: "Reinhardt LLP",
    265: "Doolittle Legal LLC",
    266: "Mignone Law Firm",
    267: "Law Office of Katia Teirstein PLLC",
    268: "Adhikari Law PLLC",
    269: "MLH Consular Consulting",
    270: "Khavinson & Associates, P.C.",
    271: "Dworsky Law Firm",
    272: "MLO Law, LLC",
    273: "TEZ Law P.C.",
    274: "Ryerson & Associates, P.C.",
    276: "SG Project Management",
    277: "Haynes Patrick Law Group",
    278: "Law Office of Isabel Cueva",
    279: "Bogle & Chang, LLC",
    280: "Law Office of Jennifer Velarde",
    282: "Kim, Song & Associates, PLLC",
    284: "Law Office of Diana Dafa, LLC",
    285: "Rose Y. Bautista Attorney at Law",
    286: "US Legal Solutions, LLC",
    287: "The Law Office of Vickie Y. Wiggins LLC"
}

paid_law_firm_ids = list(paid_law_firms.keys())


def add_dict_value(my_dict, my_key, add_value=1):
    if my_key in my_dict.keys():
        my_dict[my_key] += add_value
    else:
        my_dict[my_key] = add_value


def print_sorted_key_dict(my_dict):
    ret = ""
    for my_key in sorted(my_dict.keys()):
        ret += str(my_key) + ' ' + str(my_dict[my_key]) + "\n"
    return ret


def print_sorted_val_dict(my_dict):
    ret = ""
    sorted_value = [(v, k) for k, v in my_dict.items()]
    sorted_value.sort(reverse=True)
    for v, k in sorted_value:
        ret += str(k) + ' ' + str(v) + "\n"
    return ret


def get_case_sub_case_type_dict():
    """
    Returns 2 dictionary of case types and sub case types currently in the system
    """
    all_case_types = list(officer_step.ActionCaseType.query().iter())
    all_sub_case_types = list(officer_step.ActionSubCaseType.query().iter())
    ct_dict = {}
    sct_dict = {}
    for ct in all_case_types:
        ct_dict[ct.id] = ct.case_type
    for sct in all_sub_case_types:
        sct_dict[sct.id] = sct.name
    return ct_dict, sct_dict


def generate_report():
    case_type_dict, sub_case_type_dict = get_case_sub_case_type_dict()
    date_format = "%Y%m%d"
    # replace checkpoint argument from csrGenerator.sh
    # To run report on past date, uncomment line 202 and change the argument to the format yyyymmdd"
    
    # cp = dt.date.today()
    cp = datetime.strptime("20211205", date_format)

    check_point_end_date = cp.strftime(date_format)
    check_point_2_week = (cp - dt.timedelta(days=14)).strftime("%Y%m%d")
    check_point_3_week = (cp - dt.timedelta(days=21)).strftime("%Y%m%d")
    check_point_4_week = (cp + relativedelta(months=-1) + relativedelta(days=+1)).strftime("%Y%m%d")
    check_point_8_week = (cp + relativedelta(months=-2)).strftime("%Y%m%d")

    this_year = cp.strftime("%Y")
    case_detail_record = ""

    file1_name = "results/CSM_Report_from_{}_to_{}_snapshot_{}.txt".format(check_point_4_week, check_point_end_date,
                                                                           check_point_end_date)
    with open(file1_name, "w") as file1:
        # Use all active law firms in db to get paid vs total law firm
        # This pull query may be very inefficient
        all_law_firm = list(officer_law_office.ActionLawOffice.query().where_is_disabled_in([0]).iter())
        alf_count = len(all_law_firm)
        file1.write("CSM Report - {} \n\n".format(check_point_end_date))
        file1.write(
            "Paid Law Firm Number / Paid + Potential Law Firm Number is: {}/{} \n".format(len(paid_law_firm_ids), alf_count))
        # dangerous law firm list
        more_than_2_weeks = {}
        more_than_3_weeks = {}
        more_than_1_month = {}
        more_than_2_month = {}
        total_case, total_new_case = 0, 0
        firm_case = {}
        firm_new_case = {}
        case_type_new = {}
        case_type_year = {}
        case_type_mon_year = {}
        # dict to contain case count for sorting year/month case type lists
        dict_to_sort_year_mon = {}

        # every firm's case report
        # for fid in (paid_law_firm_ids + list(potential_law_firms.keys())):
        
        # manual rerun:
        for fid in [172]:
            firm_info = officer_law_office.ActionLawOffice.query().where_id_in([fid]).first()
            fname = firm_info.identification
            # if fid in paid_law_firm_ids:
            #     fname = paid_law_firms[fid]
            # else:
            #     fname = potential_law_firms[fid]
            firm_last_case_created_date = "00000000"
            # fetch data and update global stat
            cases = list(officer_step.ActionCase.query().where_law_firm_id_in([fid]).where_deleted_in([0]).iter())
            total_case += len(cases)
            add_dict_value(firm_case, fname, len(cases))
            firm_case_type = {}
            firm_new_case_count = 0

            firm_new_case_info = {}
            dict_to_sort_firm_new_case = {}
            for case in cases:
                # process created time into 8-digit date string in yyyymmdd
                sys_cdate = time.strftime("%b %d %Y", time.gmtime(case.created_on))
                str_year = sys_cdate[7:11]
                str_month = mon_dict[sys_cdate[:3]]
                str_day = sys_cdate[4:6]
                str_create_date = str_year + str_month + str_day
                # if manually running test date in history, ignore cases created after the day
                if str_create_date > check_point_end_date:
                    continue
                # update last case created time
                if str_create_date > firm_last_case_created_date:
                    firm_last_case_created_date = str_create_date

                # case type and sub case type
                ctid = case.case_type_id
                sctid = case.sub_case_type_id
                if ctid:
                    ct_name = case_type_dict[ctid]
                    if sctid:
                        sct_name = sub_case_type_dict[sctid]
                    else:
                        sct_name = "N/A"
                case_type_key = ct_name + '+' + sct_name
                add_dict_value(firm_case_type, case_type_key, 1)

                # time sensitive checks
                if str_year == this_year:
                    case_year_key = case_type_key + "@" + str_year
                    add_dict_value(case_type_year, case_year_key, 1)
                if check_point_8_week < str_create_date <= check_point_end_date:
                    ctmy_key = case_year_key + str_month
                    add_dict_value(case_type_mon_year, ctmy_key, 1)
                    # add two months values to organize keys by sum order
                    add_dict_value(dict_to_sort_year_mon, case_year_key, 1)
                if check_point_4_week < str_create_date <= check_point_end_date:
                    total_new_case += 1
                    firm_new_case_count += 1
                    add_dict_value(case_type_new, case_type_key, 1)
                    # increment new case stat for all record
                    # key format example: Family-based Green Card + I-130 & I-485 @ 20210614
                    new_case_date_key = case_type_key + ' @ ' + str_create_date
                    add_dict_value(dict_to_sort_firm_new_case, case_type_key, 1)
                    add_dict_value(firm_new_case_info, new_case_date_key, 1)

            # after finishing sorting all cases in a firm:
            add_dict_value(firm_new_case, fname, firm_new_case_count)

            case_detail_record += "\nFirm ID: {}, Firm Name: {}, ".format(fid, fname) \
                                  + "Total Case Number: {}, ".format(firm_case[fname]) \
                                  + "New Case Number: {}\n".format(firm_new_case[fname])
            # get lawyer list for law firm details
            case_detail_record += "Lawyer Names:  "
            partner_list = list(officer_kernel.ActionPartner.query().where_office_id_in([fid]).iter())
            lawyer_name_list = []
            for one_partner in partner_list:
                p_info = officer_kernel.ActionUser.query().where_id_in([one_partner.user_id]).first()
                pname = p_info.first_name + ' ' + p_info.last_name
                lawyer_name_list.append(pname)
            lawyer_name_list.sort()
            for one_name in lawyer_name_list:
                case_detail_record += one_name + ";  "
            case_detail_record += "\n** New Cases from {} to {}\n".format(check_point_4_week,
                                                                          check_point_end_date)
            sorted_firm = [(v, k) for k, v in dict_to_sort_firm_new_case.items()]
            sorted_firm.sort(reverse=True)
            for v, k in sorted_firm:
                dict_to_sort_firm_output = {}
            # TODO: Can this be optimized by binary search???
                for key in firm_new_case_info:
                    if k in key:
                        dict_to_sort_firm_output[key] = firm_new_case_info[key]
                case_detail_record += print_sorted_key_dict(dict_to_sort_firm_output)

            # finish processing all case for a firm, check if firm is dangerous
            if firm_last_case_created_date != '00000000':
                if check_point_3_week <= firm_last_case_created_date < check_point_2_week:
                    more_than_2_weeks[fname] = firm_last_case_created_date
                elif check_point_4_week <= firm_last_case_created_date < check_point_3_week:
                    more_than_3_weeks[fname] = firm_last_case_created_date
                elif check_point_8_week <= firm_last_case_created_date < check_point_4_week:
                    more_than_1_month[fname] = firm_last_case_created_date
                elif firm_last_case_created_date < check_point_8_week:
                    more_than_2_month[fname] = firm_last_case_created_date
            else:
                more_than_2_month[fname] = "ZERO CASE"
            # finish processing all case of one firm

        # finish processing all firms
        file1.write("\nTotal Cases in the system is {}".format(total_case))
        file1.write("\nTotal New Cases from {} to {} is {}\n".format(check_point_4_week, check_point_end_date,
                                                                     total_new_case))

        file1.write("\n********** Summary of each firm's case numbers **********\n")
        file1.write(print_sorted_val_dict(firm_case))

        file1.write("\n\n********** DANGEROUS FIRM PART **********")
        if len(more_than_1_month) == 0 and len(more_than_2_month) == 0 and len(more_than_3_weeks) == 0 and len(
                more_than_2_weeks) == 0:
            file1.write("\n** Law firms are fine and active **")
        else:
            file1.write("\n****** FIRMS MORE THAN 2 MONTHS WITHOUT NEW CASE ******\n")
            file1.write(print_sorted_key_dict(more_than_2_month))
            file1.write("\n****** FIRMS MORE THAN 1 MONTHS WITHOUT NEW CASE ******\n")
            file1.write(print_sorted_key_dict(more_than_1_month))
            file1.write("\n****** FIRMS MORE THAN 3 WEEKS WITHOUT NEW CASE ******\n")
            file1.write(print_sorted_key_dict(more_than_3_weeks))
            file1.write("\n****** FIRMS MORE THAN 2 WEEKS WITHOUT NEW CASE ******\n")
            file1.write(print_sorted_key_dict(more_than_2_weeks))
            file1.write("\n")

        file1.write("\n********** SUMMARY OF NEW CASE NUMBERS v.s. FIRMS **********\n")
        file1.write(print_sorted_val_dict(firm_new_case))

        file1.write("\n********** SUMMARY OF CASE TYPES v.s. CASE NUMBERS PER YEAR**********\n")
        file1.write(print_sorted_val_dict(case_type_year))

        file1.write("\n********* SUMMARY OF CASE TYPES v.s. CASE NUMBERS PER YEAR/MONTH **********\n")
        sorted_value = [(v, k) for k, v in dict_to_sort_year_mon.items()]
        sorted_value.sort(reverse=True)
        for v, k in sorted_value:
            dict_to_sort_output_omg = {}
            # TODO: Can this be optimized by binary search???
            for key in case_type_mon_year:
                if k in key:
                    dict_to_sort_output_omg[key] = case_type_mon_year[key]
            file1.write(print_sorted_key_dict(dict_to_sort_output_omg))

        file1.write("\n********** CASE DETAILS **********\n")
        file1.write(case_detail_record)


if __name__ == "__main__":
    global_logger.setLevel(logging.WARNING)
    generate_report()
