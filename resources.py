# -*- coding: utf-8 -*-
from varyag.officers import step as officer_step
from varyag.officers import law_office as officer_law_office
from varyag.officers import kernel as officer_kernel


DATE_FORMAT = "%Y%m%d"

mon_dict = {
    'Jan': '01',
    'Feb': '02',
    'Mar': '03',
    'Apr': '04',
    'May': '05',
    'Jun': '06',
    'Jul': '07',
    'Aug': '08',
    'Sep': '09',
    'Oct': '10',
    'Nov': '11',
    'Dec': '12'
}

test_law_firms = {
    1: "developer",
    3: "Test",
    7: "Test Law Firm",
    24: "USA Law Group",
    29: "Task Template Law Group",
    30: "AILawTest",
    38: "AddPartnerGroup",
    40: "TEST AD",
    42: "Test-Tianzesb",
    80: "Vendor Trial 01",
    112: "AILaw Support"
}

deprecated_law_firms = {
    21: "WHOmentors.com"
}

potential_law_firms = {
    92: "Frank & Delaney Immigration",
    283: "LA Immigration Service, Inc.",
    285: "Rose Y. Bautista Attorney at Law",
    286: "US Legal Solutions, LLC",
    287: "The Law Office of Vickie Y. Wiggins LLC"
}

paid_law_firms = {
    2: "DeHeng Law Offices",
    4: "LyD Law",
    5: "YC Law Group, PC",
    8: "Law Offices of Kelly H. Bu",
    10: "SAC ATTORNEYS LLP",
    16: "Carrier and Allison Law Group",
    17: "Hsiang Law Group",
    23: "LAW OFFICE OF YUANYUE MU PLLC",
    27: "Occam Immigration",
    34: "Stelmakh & Associates LLC",
    55: "Valerio Spinaci, Esq.",
    59: "JT Law Group",
    64: "Huntley PC",
    76: "Calderon Law Firm, LLC",
    83: "Ashoori Law",
    95: "Law Firm of Anish Vashistha, APLC",
    107: "Corporate Immigration Law Firm",
    109: "AFAR IMMIGRATION SERVICE",
    114: "D & T Law Corporation",
    121: "HAYMAN-WOODWARD",
    125: "Robert G. Schrader, Esq.",
    130: "Streit & Su",
    131: "The Law Firm of Edward Rodriguez, P.C",
    135: "The Mitzel Group, LLP",
    136: "Deborah J. Townsend, P.A.",
    142: "Olvera & Associates",
    144: "Knezek Law",
    145: "The Pilkington Law Firm",
    150: "Ian Li Law",
    153: "Wang, Leonard & Condon, Attorneys at Law",
    154: "Immigration Legal Options",
    155: "BBI Law Group, P.C.",
    156: "Desiree Dominguez Immigration Law Office",
    159: "Camacho   Bridges",
    161: "Comprehensive Immigration Solutions, LLC",
    162: "Path Law Group",
    163: "Murray & Silva, P.A.",
    164: "Law Office of Paul C. Agu",
    165: "Ceraulo Zhang LLP",
    167: "Elissa I. Henry Law Firm, PLLC",
    172: "Barlow & Niffen P.C.",
    174: "Alicia C. Armstrong Law Office",
    175: "Ballon Stoll Bader & Nadler, P.C.",
    178: "Law Offices of William Kiang",
    179: "Ardila Law Firm",
    180: "SBM ALLIANCE",
    181: "The Law Firm of Moumita Rahman, PLLC",
    182: "Leiva Law Firm",
    183: "Michael G. Murray, P.A.",
    184: "Moreno Law",
    185: "Kassamanian, Kodchian & Associates",
    186: "The Byars Firm",
    188: "Sodette K-M Plunkett & Associates",
    189: "MCWHIRTER LAW FIRM, PLLC",
    190: "Law Office of Isaias Torres",
    191: "Law Office Of Kisuk Paek",
    192: "Immigration Law Office of Claire Degerin",
    193: "RAMCHAND & RAVAL, P.C.",
    195: "Leleu & Associates, LLC",
    197: "Lydia Kitahara",
    198: "Berner Immigration Law Firm",
    199: "SBC ILDC",
    201: "Islas-Muñoz Law Firm",
    202: "Margaret W. Wong & Associates, LLC",
    203: "Quest It Solutions, Inc",
    204: "Ybarra Maldonado Law Group",
    207: "The Schaefer Law Firm Pllc",
    208: "The Law Office of M. Ray Arvand, P.C.",
    209: "BMM TAX SERVICES LLC",
    210: "Law Offices of Kevin R. Riva",
    212: "BMP Consulting Group",
    214: "Meng Law Group PC",
    217: "Matten Law",
    220: "Law Offices of Victoria V Kuzmina",
    221: "Francis Law Center",
    222: "Haglund Law Firm",
    223: "Robb Hill Attorney At Law PLLC",
    224: "RJS Law Group",
    227: "Moreno Colón PLLC",
    229: "Apply Usa Visa LLC",
    230: "Leaf, Ferreira, de Araujo, LLC",
    231: "Taxes David",
    232: "Benme Legal",
    235: "Rambana & Ricci, P.L.L.C.",
    238: "Jean Law Group, LLC",
    241: "Maui Immigration Law",
    242: "Hall Law Office, PLLC",
    244: "Fay Grafton Nunez",
    245: "MindQuest Technology Solutions LLC",
    248: "Pollak PLLC",
    249: "Meehan & Naveed",
    250: "Castro Legal Group",
    252: "Leone Zhgun",
    253: "Arnold L. Feldman",
    254: "The Shagin Law Group LLC",
    256: "Mirabile Law Firm",
    258: "Kasper & Frank LLP",
    259: "Gee & Zhang, llp",
    260: "Reinhardt LLP",
    265: "Doolittle Legal LLC",
    266: "Mignone Law Firm",
    267: "Law Office of Katia Teirstein PLLC",
    268: "Adhikari Law PLLC",
    269: "MLH Consular Consulting",
    270: "Khavinson & Associates, P.C.",
    271: "Dworsky Law Firm",
    272: "MLO Law, LLC",
    273: "TEZ Law P.C.",
    274: "Ryerson & Associates, P.C.",
    276: "SG Project Management",
    277: "Haynes Patrick Law Group",
    278: "Law Office of Isabel Cueva",
    279: "Bogle & Chang, LLC",
    280: "Law Office of Jennifer Velarde",
    282: "Kim, Song & Associates, PLLC",
    284: "Law Office of Diana Dafa, LLC"
}


def add_dict_value(my_dict, my_key, add_value):
    if my_key in my_dict.keys():
        my_dict[my_key] += add_value
    else:
        my_dict[my_key] = add_value


def print_sorted_key_dict(my_dict):
    ret = ""
    for my_key in sorted(my_dict.keys()):
        ret += str(my_key) + ' ' + str(my_dict[my_key]) + "\n"
    return ret


def print_sorted_val_dict(my_dict):
    ret = ""
    sorted_value = [(v, k) for k, v in my_dict.items()]
    sorted_value.sort(reverse=True)
    for v, k in sorted_value:
        ret += str(k) + ' ' + str(v) + "\n"
    return ret

def get_case_sub_case_type_dict():
    """
    Returns 2 dictionary of case types and sub case types currently in the system
    """
    all_case_types = list(officer_step.ActionCaseType.query().iter())
    all_sub_case_types = list(officer_step.ActionSubCaseType.query().iter())
    ct_dict = {}
    sct_dict = {}
    for ct in all_case_types:
        ct_dict[ct.id] = ct.case_type
    for sct in all_sub_case_types:
        sct_dict[sct.id] = sct.name
    return ct_dict, sct_dict
