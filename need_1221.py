# -*- coding: utf-8 -*-
import time
import xlsxwriter
# import pandas as pd

from varyag.officers import step as officer_step
from varyag.officers import law_office as officer_law_office

from resources import test_law_firms, get_case_sub_case_type_dict, add_dict_value, mon_dict

tlf_ids = list(test_law_firms.keys())

# Prompt: 
# 所有开过以下案件类型的律所ID，律所名称，此律所历名下历史上开过这些案件的总数量（包含删除过的案子）：
# 1. E-2 (Pre-Collection) -General,
# 2. E-2-General, 
# 3. E-2-Appeal or Motion

def get_e2_data():
    case_type_dict, sub_case_type_dict = get_case_sub_case_type_dict()
    workbook = xlsxwriter.Workbook("results/e2_summary.xlsx")
    sheet = workbook.add_worksheet("All")
    sheet.write(0, 0, "Firm ID")
    sheet.write(0, 1, "Firm Name")
    sheet.write(0, 2, "Case Type")
    sheet.write(0, 3, "Count")
    row = 1
    # Use all active law firms in db to get paid vs total law firm
    # This pull query may be very inefficient
    all_law_firm = list(officer_law_office.ActionLawOffice.query().where_is_disabled_in([0]).iter())
    for firm in all_law_firm:
        fid = firm.id

        count_dict = {}
        # if firm is test firm, skip
        if fid not in list(test_law_firms.keys()):
            fname = firm.identification

            # fetch data and update global stat
            cases = list(officer_step.ActionCase.query().where_law_firm_id_in([fid]).iter())
            # cases = list(officer_step.ActionCase.query().where_law_firm_id_in([fid]).where_deleted_in([0]).iter())

            for case in cases:
                sys_cdate = time.strftime("%b %d %Y", time.gmtime(case.created_on))
                str_year = sys_cdate[7:11]
                str_month = mon_dict[sys_cdate[:3]]
                str_day = sys_cdate[4:6]
                str_create_date = str_year + str_month + str_day
                # case type and sub case type
                ctid = case.case_type_id
                sctid = case.sub_case_type_id
                # ct_name = ""
                if ctid:
                    ct_name = case_type_dict[ctid]
                    if sctid:
                        sct_name = sub_case_type_dict[sctid]
                    else:
                        sct_name = "N/A"
                
                # only process the requested case types
                if "E-2" in ct_name or "E2" in ct_name:
                    with open("results/all_case_log.txt", "a") as txt_file:
                        txt_file.write(f"Firm ID {fid} {fname}, Case Type: {ct_name} - Sub Case Type: {sct_name} - Created On: {str_create_date} \n")
                    case_type_key = ct_name + ' + ' + sct_name
                    add_dict_value(count_dict, case_type_key, 1)
        
        # write the firm's data to Excel sheet
        # for count, case_type_name in enumerate(count_dict):
        for case_type_name in list(count_dict.keys()):
            if count_dict[case_type_name] > 0:
                sheet.write(row, 0, fid)
                sheet.write(row, 1, fname)
                sheet.write(row, 2, case_type_name)
                sheet.write(row, 3, count_dict[case_type_name])
                row += 1
    workbook.close()
            