zip: dev schema lambda.zip
	zip -rg9 lambda.zip *.py csm varyag>> zip.log
clean: clean_schema
	rm -f lambda.zip
	rm -f *.log .coverage
lambda.zip: requirements.txt
	pip3 install -r requirements.txt -t venv_lambda >> pip.log
	cd venv_lambda/ && zip -r9 ../lambda.zip * >> zip.log
	rm -rf venv_lambda
dev:
	rm -f pip.dev.log
	git config core.hooksPath git-hooks
	pip3 install -r requirements.txt --user >> pip.dev.log
	pip3 install -r requirements.dev.txt --user >> pip.dev.log
schema: clean_schema
	mkdir .schema
	cd .schema && git clone git@bitbucket.org:ailaw/varyag.git && cd varyag && make varyag/agents && make varyag/officers && mv varyag ../../
	rm -rf .schema
clean_schema:
	rm -rf .schema
	rm -rf varyag
