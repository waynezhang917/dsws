# -*- coding: utf-8 -*-
import logging
import xlsxwriter
import pandas as pd

from varyag.officers import kernel as officer_kernel
from varyag.officers import law_office as officer_law_office

logger = logging.getLogger(__name__)
global_logger = logging.getLogger("varyag")


def get_all_emails():
    # data = pd.read_excel("data/paid_Accounts_11152021.xlsx")
    # fids = list(data["Law Firm ID"])
    # fids.append(289)
    fids = [290, 292, 293, 294, 295, 296, 297, 298]
    workbook = xlsxwriter.Workbook("results/1221-emails.xlsx")
    sheet = workbook.add_worksheet("Emails")
    row = 0
    for i in fids:
        fid = int(i)
        firm_info = officer_law_office.ActionLawOffice.query().where_id_in([fid]).first()
        fname = firm_info.identification.strip()
        partner_list = list(officer_kernel.ActionPartner.query().where_office_id_in([fid]).iter())
        for p in partner_list:
            p_info = officer_kernel.ActionUser.query().where_id_in([p.user_id]).first()
            if p_info.is_disabled is None or (p_info.is_disabled is not None and not p_info.is_disabled):
                pname = p_info.name
                sheet.write(row, 0, fid)
                sheet.write(row, 1, fname)
                sheet.write(row, 2, pname)
                sheet.write(row, 3, p_info.email)
            row += 1

    workbook.close()
