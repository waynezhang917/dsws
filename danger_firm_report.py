import time
import numpy as np
import pandas as pd
from resources import get_case_sub_case_type_dict, add_dict_value

def get_charts(fid, fname, pulled_cases, out_doc):
    """
    This function is created shortly after generate_report() to address weekly danger firm report on Monday
    :param fid: firm id
    :param fname: firm name acquired in the paid_law_firms dict
    :param pulled_cases: cases object from office_steps querying
    :param out_doc: an already created docx document
    :return: none
    """
    ct_lookup, sct_lookup = get_case_sub_case_type_dict()
    case_id_list, case_created_list, case_type_list, sub_case_type_list = [], [], [], []
    case_type_count_dict = {}
    for case in pulled_cases:
        cdate = time.strftime("%Y%m%d", time.gmtime(case.created_on))
        ctid = case.case_type_id
        sctid = case.sub_case_type_id
        ct_name = ct_lookup[ctid]
        if sctid:
            sct_name = sct_lookup[sctid]
        else:
            sct_name = "N/A"
        # make df
        case_id_list.append(case.id)
        case_created_list.append(cdate)
        case_type_list.append(ct_name)
        sub_case_type_list.append(sct_name)
        add_dict_value(case_type_count_dict, ct_name, 1)
    first_date = min(case_created_list)
    last_date = max(case_created_list)
    data = {
        "case_id": case_id_list,
        "created_date": case_created_list,
        "case_type": case_type_list,
        "sub_case_type": sub_case_type_list
    }
    df = pd.DataFrame(data).sort_values(by=["created_date"])
    from_date = min(df["created_date"])
    to_date = add_date(from_date, 14)
    case_trend_dict = {}
    for one_date in df["created_date"]:
        while one_date > to_date:
            from_date = add_date(to_date, 1)
            to_date = add_date(to_date, 14)
            date_period_key = from_date + " - " + to_date
            case_trend_dict[date_period_key] = 0
        date_period_key = from_date + " - " + to_date
        case_trend_dict[date_period_key] = case_trend_dict.get(date_period_key, 0) + 1

    line_key = list(case_trend_dict.keys())
    line_val = [int(case_trend_dict[k]) for k in list(case_trend_dict.keys())]
    unique_val = list(set(line_val))
    series_df = pd.DataFrame({
        "Range": line_key,
        "Count": line_val
    })
    # two important data: series_df for line chart, case_type_count_dict for pie chart
    sns.set_theme()
    if len(line_key) <= 5:
        plt.figure(figsize=(10, 8))
    else:
        plt.figure(figsize=(25, 8))
    ax = sns.lineplot(data=series_df, x="Range", y="Count", marker="o")
    ax.tick_params(axis="x", rotation=45)
    plt.yticks(unique_val)
    plt.savefig("temp/line_chart.png", bbox_inches='tight', dpi=100)

    # pie chart
    ctkey = list(case_type_count_dict.keys())
    ctval = np.array([int(case_type_count_dict[k]) for k in ctkey])
    explode = [0.06] * len(ctkey)
    fig_pie, ax_pie = plt.subplots()

    def absolute_value(val):
        a = int(np.round(val/100*ctval.sum(), 0))
        return a

    patches, texts, autotexts = ax_pie.pie(ctval, explode=explode, labels=ctkey, autopct=absolute_value)
    for i in range(len(texts)):
        texts[i].set_fontsize(13)
    fig_pie.set_size_inches(6, 6)
    plt.savefig("temp/pie_chart.png", bbox_inches='tight', dpi=100)

    tmptxt = out_doc.add_heading("{} {} | {} to {} 开案情况".format(fid, fname, first_date, last_date), 3)
    if len(line_key) <= 5:
        out_doc.add_picture("temp/line_chart.png", width=Inches(5))
    else:
        out_doc.add_picture("temp/line_chart.png")
    out_doc.add_picture("temp/pie_chart.png", height=Inches(3.6))
    out_doc.add_page_break()
    os.remove("temp/line_chart.png")
    os.remove("temp/pie_chart.png")