# -*- coding: utf-8 -*-
import logging
import time
import xlsxwriter
import datetime as dt
from datetime import datetime

# purpose: 
# generate a csv of case info for visualization
# create a text of all cases just like in CSM, but for all time;
# csv content: case id, case type, sub case type, case time, if compiled, submitted ws? efill?

from varyag.officers import step as officer_step
from varyag.officers import kernel as officer_kernel
from gql import mon_dict, paid_law_firm_ids, potential_law_firms, paid_law_firms, get_case_sub_case_type_dict, add_dict_value

logger = logging.getLogger(__name__)
global_logger = logging.getLogger("varyag")

def gen_firm_report(fid):
    case_type_dict, sub_case_type_dict = get_case_sub_case_type_dict()
    case_detail_record = ""
    case_type_count_dict = {}
    dict_to_sort_all_case = {}

    sheet_file_name = "results/Firm_{}_Cases.xlsx".format(fname)
    header = ["Case ID", "Case Type", "Sub Case Type", "Case Created On"]
    data = []

    workbook = xlsxwriter.Workbook(sheet_file_name)
    if fid in paid_law_firm_ids:
        fname = paid_law_firms[fid]
    else:
        fname = potential_law_firms[fid]

    cases = list(officer_step.ActionCase.query().where_law_firm_id_in([fid]).where_deleted_in([0]).iter())
    
    row = 0

    for case in cases:
        case_id = case.id
        # process created time into 8-digit date string in yyyymmdd
        sys_cdate = time.strftime("%b %d %Y", time.gmtime(case.created_on))
        str_year = sys_cdate[7:11]
        str_month = mon_dict[sys_cdate[:3]]
        str_day = sys_cdate[4:6]
        str_create_date = str_year + str_month + str_day
        ctid = case.case_type_id
        sctid = case.sub_case_type_id
        if ctid:
            ct_name = case_type_dict[ctid]
        if sctid:
            sct_name = sub_case_type_dict[sctid]
        else:
            sct_name = "N/A"
        case_type_key = ct_name + '+' + sct_name
        # case_type_count_dict goes to sheet 1 for pie chart
        add_dict_value(case_type_count_dict, case_type_key, 1)
        # data goes to sheet 2 for a complete list of case info(temp)
        data.append([case_id, ct_name, sct_name, str_create_date])

    sheet1 = workbook.add_worksheet("All Case Info (Temp)")
    for entry in data:
        sheet1.write(row, 0, entry[0])
        sheet1.write(row, 1, entry[1])
        sheet1.write(row, 2, entry[2])
        sheet1.write(row, 3, entry[3])
        row += 1

    row = 0
    sheet2 = workbook.add_worksheet("Pie Data")
    for ct_info in list(case_type_count_dict.keys()):
        sheet2.write(row, 0, ct_info)
        sheet2.write(row, 1, case_type_count_dict[ct_info])
        row += 1
    workbook.close()

if __name__ == "__main__":
    global_logger.setLevel(logging.WARNING)
    gen_firm_report(188)